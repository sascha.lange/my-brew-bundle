# Description
List of installed brew packages.

# Create package list
Create a bundle file in current working directory
```shell
$ brew bundle dump
```

# Install brew packages from dump
Install packages from bundle file. File must be present in current working
directory.
```$
$ brew bundle
```

